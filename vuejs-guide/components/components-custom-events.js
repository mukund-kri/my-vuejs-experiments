Vue.component('child', {
  template: '#child',
  data: function() { return {msg: 'hello'} },

  methods: {
    'sendMessage': function() {
      this.$dispatch('new-message', this.msg);
      this.msg = '';
    }
  }
});



new Vue({
  el: "#main",
  data: {
    messages: []
  },
  events: {
    'new-message': function(msg) {
      // This method is triggered when this component gets a 'new-message'
      // signal. This add the message onto the messages list.
      this.messages.push(msg);
    }
  },

  methods: {
    handleNewMessage: function(msg) {
      // explicity spacify the handling method on 'new-message' event.
      this.messages.push(msg);
    }
  }
});
