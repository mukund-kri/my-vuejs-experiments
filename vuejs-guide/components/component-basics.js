
// Custom component
var Component1 = Vue.extend({
  template: "<h3>Hello Components :: Component 1</h3>"
});


// Local registration
var LocalChild = Vue.extend({
  template: '<h3>In component local child</h3>'
});


var LocalChildExample = Vue.extend({
  template: '#local-reg',
  components: {
    'local-child': LocalChild
  }
});


// Registration sugar. Pass in the options insted of the whole
// component
var RegistrationSugar = Vue.extend({
  template: "<div><child-component-sugar></child-component-sugar></div>",
  components: {
    'child-component-sugar': {
      template: "<p>Child component. Sugar Syntax. A var = {{ avar }}</p>",
      // do not do this. Data is shared with scope outside the component
      // data: data

      // insted do this
      data: function() {
        return { avar: 213 }
      }
    }
  }
});

Vue.component('custom1', Component1);
Vue.component('local-child-example', LocalChildExample);
Vue.component('registration-sugar', RegistrationSugar);

new Vue({
  el: "#simple",
  data: { msg: "Vue 1" }
});
