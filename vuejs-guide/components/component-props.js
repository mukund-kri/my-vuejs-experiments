var Child1 = Vue.extend({
  template: '#child1-template',
  props: ['msg', 'sharedVar1', 'sharedVar2']
});

// The root component
new Vue({
  el: '#main',
  data: {
    pSharedVar1: "abc",
    pSharedVar2: "second"
  },
  components: {
    'child1': Child1
  }
});
