new Vue({
  el: "#main",
  data: {
    selectedComponent: 'home'
  },
  components: {
    'home': { template: 'home page' },
    'about': { template: 'about page' },
    'contact': { template: 'contact us' }
  }
});
