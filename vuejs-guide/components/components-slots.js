
Vue.component('no-slot', {
  template: '<h2>No slot component</h2><p>Parent content overwritten</p>'
})

Vue.component('default-slot', {
  template: "#default-slot"
});

Vue.component('multiple-slot', {
  template: "#multiple-slot"
})

new Vue({
  el: "#main"
});
