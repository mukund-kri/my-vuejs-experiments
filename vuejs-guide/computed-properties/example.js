new Vue({
  el: "#main",
  data: {
    firstName: "Bob",
    lastName: "Doe",

    kelvin: 0.0,
  },
  computed: {
    fullName: function() {
      return this.firstName + " " + this.lastName;
    },

    centigrade: {
      cache: false,
      set: function(newValue) {
        this.kelvin = parseInt(newValue) + 273.15;
      },
      get: function() {
        return this.kelvin - 273.15;
      }
    }
  }
});
