new Vue({
  el: "#main",
  data: {
    descriptor: 'item no.',
    items: [
      {_id: '1', name: 'one'},
      {_id: '2', name: 'two'},
      {_id: '3', name: 'three'}
    ],
    newItem: {
      _id: '',
      name: ''
    }
  },

  methods: {
    addNewItem: function() {
      this.items.push(this.newItem);
      this.newItem = {_id: '', name: ''};
    },
    removeItem: function(idx) {
      this.items.splice(idx, 1);
    }
  }
});
