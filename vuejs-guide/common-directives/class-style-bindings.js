new Vue ({
  el: "#main",
  data: {
    isA: true,
    isB: false,

    classObj: {
      'class-c': true,
      'class-d': false
    },

    class1: 'class-a',
    class2: 'class-d',

    bColor: '#FFCCCC',
    fColor: '#006633',

    // Full class object
    styleObject: {
      'background-color': '#66CCCC',
      'color': '#330000'
    },

    txtSize: {
      'font-size': '20px'
    }
  }
});
