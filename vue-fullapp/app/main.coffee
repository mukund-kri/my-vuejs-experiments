Vue = require 'vue'
App = require './components/App.vue'

VueRouter = require 'vue-router'
VueResource = require 'vue-resource'


# register plugins
Vue.use VueRouter
Vue.use VueResource


# create a new router
router = new VueRouter()

router.map
  '/foo':
    component:
      template: "<h3>Foo</h3>"

# The root component`
App = Vue.extend
  components:
    app: App

router.start(App, 'body')
