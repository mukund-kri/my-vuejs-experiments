path    = require 'path'
webpack = require 'webpack'


config =

  entry: './main.coffee'

  output:
    path: __dirname
    filename: 'build.js'

  module:
    loaders: [
      test: /\.coffee$/
      loader: "coffee-loader"
    ,
      test:  /\.vue$/
      loader: 'vue'
    ,
      test: /\.js$/,
      loader: 'babel',
      exclude: /node_modules/
    ]

  resolve:
    extensions: ["", ".web.coffee", ".web.js", ".coffee", ".js", ".vue"]

module.exports = config
